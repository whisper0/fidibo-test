<?php

namespace App\Modules\BookPublisher\Repositories;

use App\Models\Book;
use App\Models\BooksPublishers;
use Illuminate\Support\Facades\Cache;

class BookPublisherRepository implements BookPublisherRepositoryInterface
{
    public function getPublishers(Book $book)
    {
        return Cache::remember('publishers:book:' . $book->id, 600, function () use ($book) {
            return BooksPublishers::join('publishers', 'publishers.id', 'books_publishers.id')
                ->where('books_publishers.book_id', $book->id)
                ->select('books_publishers.id', 'books_publishers.publisher_id', 'publishers.name')
                ->get();
        });
    }
}
