<?php

namespace App\Modules\BookPublisher\Repositories;

use App\Models\Book;

interface BookPublisherRepositoryInterface
{
    public function getPublishers(Book $book);
}
