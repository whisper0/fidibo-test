<?php

namespace App\Modules\BookPublisher\Services;

use App\Models\Book;

interface BookPublisherServiceInterface
{
    public function bindData(Book $book);
}
