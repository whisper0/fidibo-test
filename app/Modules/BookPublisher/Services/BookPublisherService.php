<?php

namespace App\Modules\BookPublisher\Services;

use App\Models\Book;
use App\Modules\BookPublisher\Repositories\BookPublisherRepositoryInterface;

class BookPublisherService implements BookPublisherServiceInterface
{
    protected $bookPublisherRepository;

    function __construct(BookPublisherRepositoryInterface $bookPublisherRepository)
    {
        $this->bookPublisherRepository = $bookPublisherRepository;
    }

    public function bindData(Book $book)
    {
        $book->publishers = $this->bookPublisherRepository->getPublishers($book);

        return $book;
    }
}
