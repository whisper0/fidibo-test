<?php

namespace App\Modules\BookAuthor\Repositories;

use App\Models\Book;

interface BookAuthorRepositoryInterface
{
    public function getAuthors(Book $book);
}
