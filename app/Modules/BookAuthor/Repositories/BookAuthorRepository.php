<?php

namespace App\Modules\BookAuthor\Repositories;

use App\Models\Book;
use App\Models\BooksAuthors;
use Illuminate\Support\Facades\Cache;

class BookAuthorRepository implements BookAuthorRepositoryInterface
{
    public function getAuthors(Book $book)
    {
        return Cache::remember('authors:book:' . $book->id, 600, function () use ($book) {
            return BooksAuthors::join('authors', 'authors.id', 'books_authors.id')
                ->where('books_authors.book_id', $book->id)
                ->select('books_authors.id', 'books_authors.author_id', 'authors.name')
                ->get();
        });
    }
}
