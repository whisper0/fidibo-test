<?php

namespace App\Modules\BookAuthor\Services;

use App\Models\Book;
use App\Modules\BookAuthor\Repositories\BookAuthorRepositoryInterface;

class BookAuthorService implements BookAuthorServiceInterface
{
    protected $bookAuthorRepository;

    function __construct(BookAuthorRepositoryInterface $bookAuthorRepository)
    {
        $this->bookAuthorRepository = $bookAuthorRepository;
    }

    public function bindData(Book $book)
    {
        $book->authors = $this->bookAuthorRepository->getAuthors($book);

        return $book;
    }
}
