<?php

namespace App\Modules\BookAuthor\Services;

use App\Models\Book;

interface BookAuthorServiceInterface
{
    public function bindData(Book $book);
}
