<?php

namespace App\Modules\Book\Services;

use App\Modules\Book\Repositories\BookRepositoryInterface;
use App\Modules\BookAuthor\Services\BookAuthorServiceInterface;
use App\Modules\BookPublisher\Services\BookPublisherServiceInterface;

class BookService implements BookServiceInterface
{
    protected $bookRepository;
    protected $bookAuthorService;
    protected $bookPublisherService;

    function __construct(BookRepositoryInterface $bookRepository, BookAuthorServiceInterface $bookAuthorService, BookPublisherServiceInterface $bookPublisherService)
    {
        $this->bookRepository = $bookRepository;
        $this->bookAuthorService = $bookAuthorService;
        $this->bookPublisherService = $bookPublisherService;
    }

    public function search(?string $keyword, int $pageSize = 10)
    {
        $books = $this->bookRepository->search($keyword, $pageSize);

        foreach ($books as $book) {
            $book = $this->bookAuthorService->bindData($book);
            $book = $this->bookPublisherService->bindData($book);
        }

        return $books;
    }
}
