<?php

namespace App\Modules\Book\Services;

interface BookServiceInterface
{
    public function search(?string $keyword, int $pageSize = 10);
}
