<?php

namespace App\Modules\Book\Repositories;

use App\Models\Book;
use Illuminate\Support\Facades\Cache;

class BookRepository implements BookRepositoryInterface
{
    public function search(?string $keyword, int $pageSize = 10)
    {
        $books = Cache::remember("books:keyword:{$keyword}:page:size:{$pageSize}", 600, function () use ($keyword, $pageSize) {
            $books = Book::select('id','title', 'slug', 'image_name', 'content');

            if ($keyword) {
                $books->where('title', 'LIKE', "%{$keyword}%")
                    ->orWhere('content', 'LIKE', "%{$keyword}%");
            }

            $books = $books->orderBy('id', 'desc')
                ->paginate($pageSize);

            return $books;
        });

        return $books;
    }
}
