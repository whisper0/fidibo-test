<?php

namespace App\Modules\Book\Repositories;

interface BookRepositoryInterface
{
    public function search(?string $keyword, int $pageSize = 10);
}
