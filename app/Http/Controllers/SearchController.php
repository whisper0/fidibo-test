<?php

namespace App\Http\Controllers;

use App\Modules\Book\Services\BookServiceInterface;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    protected $bookSevice;

    function __construct(BookServiceInterface $bookService)
    {
        $this->bookSevice = $bookService;
    }

    public function searchBooksByKeyword(Request $request)
    {
        $this->validate($request, [
            'keyword' => 'nullable|string',
        ]);

        $books = $this->bookSevice->search($request->keyword, 15);

        return response()->json([
            'books' => $books,
        ]);
    }
}
