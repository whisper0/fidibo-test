<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function register(Request $request)
    {
        $data = $this->validate($request, [
            'name' => 'required|string',
            'email' => 'required|string|email',
            'password' => 'required|string',
        ]);

        $data['password'] = bcrypt($data['password']);
        $user = User::create($data);

        $accessToken = $user->createToken('authToken')->accessToken;

        return response()->json([
            'user' => $user,
            'access_token' => $accessToken,
        ]);
    }

    public function login(Request $request)
    {
        $data = $this->validate($request, [
            'email' => 'required|string',
            'password' => 'required|string',
        ]);

        if (!Auth::attempt($data)) {
            return response()->json([
                'message' => 'invalid email or password'
            ], 401);
        }

        $user = auth()->user();
        $accessToken = $user->createToken('authToken')->accessToken;;
        return response()->json([
            'user' => Auth::user(),
            'access_token' => $accessToken,
        ]);
    }

    public function logout()
    {
        auth()->user()->token()->revoke();
        return response()->json([
            'message' => 'logged out!'
        ]);
    }

    public function user()
    {
        return response()->json([
            'user' => auth()->user(),
        ]);
    }
}
