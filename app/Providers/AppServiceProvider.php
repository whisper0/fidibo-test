<?php

namespace App\Providers;

use App\Modules\Book\Repositories\BookRepository;
use App\Modules\Book\Repositories\BookRepositoryInterface;
use App\Modules\Book\Services\BookService;
use App\Modules\Book\Services\BookServiceInterface;
use App\Modules\BookAuthor\Repositories\BookAuthorRepository;
use App\Modules\BookAuthor\Repositories\BookAuthorRepositoryInterface;
use App\Modules\BookAuthor\Services\BookAuthorService;
use App\Modules\BookAuthor\Services\BookAuthorServiceInterface;
use App\Modules\BookPublisher\Repositories\BookPublisherRepository;
use App\Modules\BookPublisher\Repositories\BookPublisherRepositoryInterface;
use App\Modules\BookPublisher\Services\BookPublisherService;
use App\Modules\BookPublisher\Services\BookPublisherServiceInterface;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(BookRepositoryInterface::class, BookRepository::class);
        $this->app->bind(BookServiceInterface::class, BookService::class);

        $this->app->bind(BookAuthorRepositoryInterface::class, BookAuthorRepository::class);
        $this->app->bind(BookAuthorServiceInterface::class, BookAuthorService::class);

        $this->app->bind(BookPublisherRepositoryInterface::class, BookPublisherRepository::class);
        $this->app->bind(BookPublisherServiceInterface::class, BookPublisherService::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
