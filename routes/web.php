<?php

use Illuminate\Support\Facades\Route;

Route::get('/login', function () {
    return response()->json([
        'message' => 'you need to login (set header Accept to application/json)'
    ],401);
})->name('login');
