<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class AuthTest extends TestCase
{
    use RefreshDatabase;

    public function test_register_success()
    {
        \Artisan::call('passport:install --force');

        $response = $this->json(
            'POST',
            'api/register',
            [
                'name' => 'test',
                'email' => 'test@yahoo.com',
                'password' => 'secret'
            ],
            ['Accept' => 'application/json']
        );
        $response->assertStatus(200);
    }

    public function test_login_success()
    {
        \Artisan::call('passport:install --force');

        $password = 'secret';

        $user = User::create([
            'name' => 'test',
            'email' => 'email@yahoo.com',
            'password' =>bcrypt($password),
        ]);
        

        $response = $this->json(
            'POST',
            'api/login',
            ['email' => $user->email, 'password'=> $password],
            ['Accept' => 'application/json']
        );
        $response->assertStatus(200);
    }

    public function test_invalid_login_credential_fail()
    {
        \Artisan::call('passport:install --force');

        $password = 'secret';

        $user = User::create([
            'name' => 'test',
            'email' => 'email@yahoo.com',
            'password' =>bcrypt($password),
        ]);
        

        $response = $this->json(
            'POST',
            'api/login',
            ['email' => $user->email, 'password'=> 'wrong password'],
            ['Accept' => 'application/json']
        );
        $response->assertStatus(401);
    }

    public function test_logout_success()
    {
        \Artisan::call('passport:install --force');

        $user = User::factory()->create();
        $accessToken = $user->createToken('authToken')->accessToken;

        $response = $this->json('GET', 'api/logout', [], ['Accept' => 'application/json', 'Authorization' => 'Bearer '.$accessToken]);
        $response->assertStatus(200);
    } 

    public function test_get_current_user_success()
    {
        \Artisan::call('passport:install --force');

        $user = User::factory()->create();
        $accessToken = $user->createToken('authToken')->accessToken;

        $response = $this->json('GET', 'api/user', [], ['Accept' => 'application/json', 'Authorization' => 'Bearer '.$accessToken]);
        $response->assertStatus(200);
    } 

    public function test_middleware_working()
    {
        \Artisan::call('passport:install --force');

        $response = $this->json('GET', 'api/user', [], ['Accept' => 'application/json', 'Authorization' => 'Bearer '.'Wrong Token is here']);
        $response->assertStatus(401);
    } 
}
