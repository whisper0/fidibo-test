<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class SearchBookTest extends TestCase
{
    use RefreshDatabase;

    public function test_fail_with_no_access_token()
    {
        $response = $this->json('POST', 'api/search/book', ['keyword' => 'keyword'], ['Accept' => 'application/json']);
        $response->assertStatus(401);
    }

    public function test_search_success()
    {
        \Artisan::call('passport:install --force');

        $user = User::factory()->create();
        $accessToken = $user->createToken('authToken')->accessToken;

        $response = $this->json('POST', 'api/search/book', ['keyword' => 'keyword'], ['Accept' => 'application/json', 'Authorization' => 'Bearer '.$accessToken]);
        $response->assertStatus(200);
    }
}
