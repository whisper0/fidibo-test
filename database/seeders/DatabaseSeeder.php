<?php

namespace Database\Seeders;

use App\Models\BooksPublishers;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            BooksTtableSeeder::class,
            AuthorsTableSeeder::class,
            PublishersTableSeeder::class,
            BooksPublishersTableSeeder::class,
            BooksAuthorsTableSeeder::class,
        ]);
    }
}
