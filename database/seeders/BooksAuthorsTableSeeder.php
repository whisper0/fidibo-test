<?php

namespace Database\Seeders;

use App\Models\BooksAuthors;
use Illuminate\Database\Seeder;

class BooksAuthorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        BooksAuthors::factory(11)->create();
    }
}
