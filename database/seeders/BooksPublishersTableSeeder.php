<?php

namespace Database\Seeders;

use App\Models\BooksPublishers;
use Illuminate\Database\Seeder;

class BooksPublishersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        BooksPublishers::factory(11)->create();
    }
}
