<?php

namespace Database\Factories;

use App\Models\Author;
use App\Models\Book;
use App\Models\BooksAuthors;
use Illuminate\Database\Eloquent\Factories\Factory;

class BooksAuthorsFactory extends Factory
{
    protected $model = BooksAuthors::class;
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'book_id' => Book::factory(),
            'author_id' => Author::factory()
        ];
    }
}
