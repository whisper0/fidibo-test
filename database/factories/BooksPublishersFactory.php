<?php

namespace Database\Factories;

use App\Models\Book;
use App\Models\BooksPublishers;
use App\Models\Publisher;
use Illuminate\Database\Eloquent\Factories\Factory;

class BooksPublishersFactory extends Factory
{
    protected $model = BooksPublishers::class;
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'book_id' => Book::factory(),
            'publisher_id' => Publisher::factory()
        ];
    }
}
