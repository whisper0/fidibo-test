<?php

namespace Database\Factories;

use App\Models\Book;
use Illuminate\Database\Eloquent\Factories\Factory;

class BookFactory extends Factory
{
    protected $model = Book::class;
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $word = $this->faker->word();
        return [
            'title' =>  $word,
            'slug' =>  $word,
            'image_name' => 'https://picsum.photos/200',
            'content' =>  $this->faker->paragraph(),
        ];
    }
}
